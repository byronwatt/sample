

```plantuml
actor hostmsg
actor DEFECT_RSRC_TRANS
actor DEFECTS
control MT_MUTEX_DEFECT_RSRC_MAP_TASK
database MT_EVENT_DEFECT_RSRC_MAP_UPDATE

activate hostmsg #FBB4AE
hostmsg -> MT_EVENT_DEFECT_RSRC_MAP_UPDATE ++ #green : event_post() sets the flag to 1
MT_EVENT_DEFECT_RSRC_MAP_UPDATE --> hostmsg
activate DEFECT_RSRC_TRANS #FED9A6
deactivate MT_EVENT_DEFECT_RSRC_MAP_UPDATE
DEFECT_RSRC_TRANS -> MT_EVENT_DEFECT_RSRC_MAP_UPDATE ++ #red : event_wait clears the flag to 0 (event_ready now returns 0!)\n but we haven't grabbed the mutex or transfered the resource map yet!
activate DEFECTS #CCEBC5
DEFECTS -> MT_MUTEX_DEFECT_RSRC_MAP_TASK ++ : lock
MT_MUTEX_DEFECT_RSRC_MAP_TASK --> DEFECTS : locked
DEFECTS -> MT_EVENT_DEFECT_RSRC_MAP_UPDATE : check if event_ready() == 0 (yes !)
MT_EVENT_DEFECT_RSRC_MAP_UPDATE --> DEFECTS : defects task thinks resource map has been transfered

DEFECTS -> DEFECTS : set ipi bit

DEFECTS -> MT_MUTEX_DEFECT_RSRC_MAP_TASK : unlock
MT_MUTEX_DEFECT_RSRC_MAP_TASK --> DEFECTS -- : unlocked

DEFECT_RSRC_TRANS -> MT_MUTEX_DEFECT_RSRC_MAP_TASK ++ : lock
MT_MUTEX_DEFECT_RSRC_MAP_TASK --> DEFECT_RSRC_TRANS : locked
DEFECT_RSRC_TRANS -> DEFECT_RSRC_TRANS : transfer resource map to host
DEFECT_RSRC_TRANS -> MT_MUTEX_DEFECT_RSRC_MAP_TASK : unlock
MT_MUTEX_DEFECT_RSRC_MAP_TASK --> DEFECT_RSRC_TRANS -- : unlocked


hide footbox
```


With old freertos labs it didn't work:

```plantuml
actor hostmsg
actor "thread_a\nhigh priority" as high_priority_thread
actor "thread_b\nlow priority" as low_priority_thread
control gdma
activate hostmsg #FBB4AE
hostmsg -> gdma ++ #FBB4AE : acquire ch 1
return ch1 granted
hostmsg -> gdma ++ #FBB4AE : acquire ch 2
return ch2 granted
hostmsg -> high_priority_thread ++ #FED9A6 : start thread a (high priority)
hostmsg -> low_priority_thread ++ #CCEBC5 : start thread b (low priority)
hostmsg -> hostmsg : sleep a smidge
high_priority_thread -> gdma ++ #FED9A6 : acquire ch 1
low_priority_thread -> gdma ++ #CCEBC5 : acquire ch 2

hostmsg -> gdma : release ch 2
gdma -> high_priority_thread : retest if ch 1 is available (not)
gdma -> high_priority_thread : retest if ch 1 is available (not)
... low priority thread never awakened to retest if ch2 available ...
hostmsg -> low_priority_thread : wait for low priority thread to finish
hostmsg -> gdma : release ch 1
hostmsg -> hostmsg : wait for high priority thread to finish
hide footbox
```

With current code it should work properly:

```plantuml
actor hostmsg
actor "thread_a\nhigh priority" as high_priority_thread
actor "thread_b\nlow priority" as low_priority_thread
control gdma
activate hostmsg #FBB4AE
hostmsg -> gdma ++ #FBB4AE : acquire ch 1
return ch1 granted
hostmsg -> gdma ++ #FBB4AE : acquire ch 2
return ch2 granted
hostmsg -> high_priority_thread ++ #FED9A6 : start thread a (high priority)
hostmsg -> low_priority_thread ++ #CCEBC5 : start thread b (low priority)
hostmsg -> hostmsg : sleep a smidge
high_priority_thread -> gdma ++ #FED9A6 : acquire ch 1
low_priority_thread -> gdma ++ #CCEBC5 : acquire ch 2

hostmsg -> gdma : release ch 2
gdma -> high_priority_thread : retest if ch 1 is available (not)
gdma -> low_priority_thread -- : retest if ch 2 is available (yes!)
hostmsg -> low_priority_thread : wait for low priority thread to finish
low_priority_thread -> hostmsg -- : finished
hostmsg -> gdma : release ch 1
gdma -> high_priority_thread -- : retest if ch 1 is available (yes!)
hostmsg -> high_priority_thread : wait for high priority thread to finish
high_priority_thread -> hostmsg -- : finished
hide footbox
```